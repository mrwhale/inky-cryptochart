# inky-cryptochart

CLI tool to display cryptocurrency price graph on [Pimoroni Inky pHAT](https://shop.pimoroni.com/products/inky-phat)

![](https://gitlab.com/mrwhale/inky-cryptochart/-/raw/master/inky.png)
![](https://gitlab.com/mrwhale/inky-cryptochart/-/raw/master/inky-crypto-pi.png)

04b03 freeware font by [04](http://www.04.jp.org/).
Originally forked from [DurandA](https://github.com/DurandA/inky-cryptochart)

## Setup

Install the `inky-phat` prerequisites (see [official instructions](https://github.com/pimoroni/inky-phat#installing)):

```
sudo apt-get install python3-pip python3-dev
```

Install required packages:

```
sudo pip3 install -r requirements.txt
```

## Usage

```
usage: index.py [-h] [--pair PAIR] [--flip] [--output OUTPUT]

optional arguments:
  -h, --help       show this help message and exit
  --pair PAIR      currency pair (default: BTCUSDT)
  --flip           rotate the display (default: False)
  --output OUTPUT  save plot as png (default: None)
```

## Configure crontab

Use `crontab -e` to add run the cronjob every 15 minutes:

```
*/15 * * * * (cd /home/george/inky-cryptochart; python3 index.py --pair BTCUSDT --flip)
```

If you are not familiar with cron, have a look at the excellent [CronHowto](https://help.ubuntu.com/community/CronHowto) wiki to configure it according to your requirements.
