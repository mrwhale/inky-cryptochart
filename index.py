#!/usr/bin/env python3

#from cryptochart import ax, fig, w, h, dpi
from cryptochart.providers import quotes_historical_binance_ohlc
import argparse, io
from datetime import datetime, timedelta
try:
    from mpl_finance import candlestick_ohlc
except ImportError:
    from matplotlib.finance import candlestick_ohlc
from PIL import Image, ImageDraw, ImageFont
from datetime import datetime, timedelta
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import font_manager

#TODO allow this to be adjustable based on the device you have. InkyPHAT or InkyWHAT
w, h = (212, 104)
dpi = 144
fig, ax = plt.subplots(figsize=(212/dpi, 104/dpi), dpi=dpi)

#TODO figure out how many digits in the price and adjust left margin accordingly
# .14 for 3, .15 for 4, .16 for 5
fig.subplots_adjust(top=1, bottom=0, left=0.16, right=1)

ticks_font = font_manager.FontProperties(fname='04B_03__.TTF', size=4)
plt.rcParams['text.antialiased'] = False

for label in ax.get_yticklabels() :
    label.set_fontproperties(ticks_font)
ax.yaxis.set_tick_params(pad=2, width=1)

#TODO work on getting date/time on xaxis
ax.xaxis.set_ticks([])
ax.set_frame_on(False)

#TODO Add argument to choose provider. Binance or kraken
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--pair", default='BTCUSDT', help="currency pair")
parser.add_argument('--flip', dest='flip', action='store_true', help="rotate the display")
parser.set_defaults(flip=False)
parser.add_argument("--output", help="save plot as png")
parser.add_argument("--interval", default='30m', help="Ticker interval")
args = parser.parse_args()

yesterday = datetime.now() - timedelta(hours=12)

quotes = quotes_historical_binance_ohlc(args.pair, yesterday, args.interval)
if len(quotes) == 0:
    raise SystemExit

#candlestick_ohlc(ax, quotes, width=1)
values = []
dates = []
for i in quotes:
  values.append(i[4])
  dates.append(i[0])

ax.plot(dates,values, color='black', linewidth=1)
ax.xaxis_date()
ax.autoscale_view()

ymin, ymax = ax.get_ylim()

last_low = quotes[-1][3]
last_high = quotes[-1][2]
last_close = quotes[-1][4]

font = ImageFont.truetype('04B_03__.TTF', 8)
fontnumber = ImageFont.truetype('UbuntuMono-R.ttf', 12)

with io.BytesIO() as f:
    fig.savefig(f, dpi=dpi, cmap="bwr", interpolation="none", origin="lower", pad_inches=0)#bbox_inches='tight')
    f.seek(0)
    i = Image.open(f).convert('P')
    d = ImageDraw.Draw(i)
    ypos = 0 if ymax - last_high > last_low - ymin else h - 6
    yposnumber = 0 if ymax - last_high > last_low - ymin else h - 10


    if not args.output:
        from inky import InkyPHAT
        inkyphat = InkyPHAT('black')
        d.text((150,ypos), args.interval, inkyphat.BLACK, font)
        d.text((170, yposnumber), '{:.2f}'.format(last_close), inkyphat.BLACK, fontnumber)
        d.text((0,98), args.pair, inkyphat.BLACK,font)
        inkyphat.set_image(i)
    if args.flip:
        rot = i.rotate(180)
        inkyphat.set_image(rot)

    if args.output:
        d.text((150,ypos), args.interval,fill=1,font=font)
        d.text((170, yposnumber), '{:.2f}'.format(last_close), fill=1,font=fontnumber)
        d.text((0,98), args.pair, fill=1,font=font)
        i.save(args.output)
        raise SystemExit
    
    inkyphat.show()
